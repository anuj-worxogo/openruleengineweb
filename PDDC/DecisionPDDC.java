/*
This file is part of Worxogo Rule Engine Wrapper(wre) which is a java webapp.
Copyright © 2016 Worxogo Solutions Pvt Ltd

wre is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

wre is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>. 
 */
package PDDC;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.openrules.ruleengine.Decision;

@WebServlet("/DecisionPDDC")
public class DecisionPDDC extends HttpServlet {	
	public static String config_path = "/WEB-INF/properties";
	public static String config_file = "config.properties";
	private static Logger logger=Logger.getLogger("gSales");
	public static String log4j_file = "log4j.properties";
	
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		FileInputStream input;
		try{			
			Properties prop = new Properties();
			String cfgPath = req.getServletContext().getRealPath(config_path);
			input = new FileInputStream(cfgPath+File.separator+config_file);
			prop.load(input);
			String log4jConfigFile = cfgPath+File.separator+log4j_file;
			PropertyConfigurator.configure(log4jConfigFile);
			
			String path = prop.getProperty("PATH");
			logger.debug("path "+path);
			
			logger.debug("cfgPath "+cfgPath);
			
					
			String file = prop.getProperty("PDDC_FILE");		
			
			
			String fileName = "file:"+req.getServletContext().getRealPath(path) +File.separator+ file;
			Decision decision = new Decision("Main",fileName);			
			
			
			int points = executeDecision(Double.parseDouble(req.getParameter("pddc")), decision);
			res.setHeader("Points", ""+points);				
			
		}
		catch(Exception e){
			e.printStackTrace();			
		}
	}
	
	public int executeDecision(double value, Decision decision){		
		  SCE sce = new SCE();
		  sce.setPddc(value);
		  decision.put("sce", sce);
		  Points points = new Points();
		  decision.put("points",points);		  
		  decision.saveRunLog(true);
		  decision.execute();
		  logger.debug("PDDC Points "+points.getPddcPoints());
		  return points.getPddcPoints();			 
	}
}
