/*
This file is part of Worxogo Rule Engine Wrapper(wre) which is a java webapp.
Copyright © 2016 Worxogo Solutions Pvt Ltd

wre is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

wre is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>. 
 */
package SalesLeaderboard;

public class SCE {
	String sceName;
	String sceCode;
	double enquiryPoints;
	double beatPlanPoints;
	double pddcPoints;
	double loginPoints;
	double weekOfMonthPoints;
	double ftrAvailabilityPoints;
	double caseDisbursedPoints;
	double tatLtoSPoints;
	double tatStoDPoints;
	public String getSceName() {
		return sceName;
	}
	public void setSceName(String sceName) {
		this.sceName = sceName;
	}
	public String getSceCode() {
		return sceCode;
	}
	public void setSceCode(String sceCode) {
		this.sceCode = sceCode;
	}
	public double getEnquiryPoints() {
		return enquiryPoints;
	}
	public void setEnquiryPoints(double enquiryPoints) {
		this.enquiryPoints = enquiryPoints;
	}
	public double getBeatPlanPoints() {
		return beatPlanPoints;
	}
	public void setBeatPlanPoints(double beatPlanPoints) {
		this.beatPlanPoints = beatPlanPoints;
	}
	public double getPddcPoints() {
		return pddcPoints;
	}
	public void setPddcPoints(double pddcPoints) {
		this.pddcPoints = pddcPoints;
	}
	public double getLoginPoints() {
		return loginPoints;
	}
	public void setLoginPoints(double loginPoints) {
		this.loginPoints = loginPoints;
	}
	public double getWeekOfMonthPoints() {
		return weekOfMonthPoints;
	}
	public void setWeekOfMonthPoints(double weekOfMonthPoints) {
		this.weekOfMonthPoints = weekOfMonthPoints;
	}
	public double getFtrAvailabilityPoints() {
		return ftrAvailabilityPoints;
	}
	public void setFtrAvailabilityPoints(double ftrAvailabilityPoints) {
		this.ftrAvailabilityPoints = ftrAvailabilityPoints;
	}
	public double getCaseDisbursedPoints() {
		return caseDisbursedPoints;
	}
	public void setCaseDisbursedPoints(double caseDisbursedPoints) {
		this.caseDisbursedPoints = caseDisbursedPoints;
	}
	public double getTatLtoSPoints() {
		return tatLtoSPoints;
	}
	public void setTatLtoSPoints(double tatLtoSPoints) {
		this.tatLtoSPoints = tatLtoSPoints;
	}
	public double getTatStoDPoints() {
		return tatStoDPoints;
	}
	public void setTatStoDPoints(double tatStoDPoints) {
		this.tatStoDPoints = tatStoDPoints;
	}
	
}
