Two URLs are exposed

/wre/RuleEngineServlet?reload=true - to reload the decision objects and delete any of the runtime generated classes.
/wre/RuleEngineServlet?clientName=<clientName>&requestName=<RuleFileName>&param1=<param1Name>&value1=<value1>....

#We use open rules engine Java code generation utility, so Tomcat needs to load  the newly generated classes.
##Update conf/context.xml in tomcat configuration.


	<!-- web application will be reloaded. -->
    <WatchedResource>WEB-INF/classes/</WatchedResource>
    <WatchedResource>WEB-INF/web.xml</WatchedResource>
   <WatchedResource>${catalina.base}/conf/web.xml</WatchedResource>

    <!-- Uncomment this to disable session persistence across Tomcat restarts -->



