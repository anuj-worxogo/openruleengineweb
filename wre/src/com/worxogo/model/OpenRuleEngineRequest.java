/*
This file is part of Worxogo Rule Engine Wrapper(wre) which is a java webapp.
Copyright © 2016 Worxogo Solutions Pvt Ltd

wre is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

wre is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>. 
 */
package com.worxogo.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.worxogo.ruleengine.OpenRulesTaskRunner;

/**
 * Request Object to be passed to OpenRulesEngineRunner.execute()
 * 
 * @author anuj
 *
 */
public class OpenRuleEngineRequest implements IRuleEngineRequest {

	private static Logger _logger = LogManager
			.getLogger(OpenRuleEngineRequest.class);

	private String clientName = "";

	private String requestName;

	private Map<String, String> requestParams;

	public OpenRuleEngineRequest() {

		requestParams = new HashMap<String, String>();
	}

	/**
	 * @param clientName
	 * @param requestName
	 * @param requestParams
	 */
	public OpenRuleEngineRequest(String clientName, String requestName,
			Map<String, String> requestParams) {
		if (requestName == null || requestName.trim().equalsIgnoreCase("")) {
			_logger.debug("Invalid Request : " + this);
			throw new RuntimeException("Invalid Request : " + this);
		}

		this.clientName = clientName;
		this.requestName = requestName;
		this.requestParams = requestParams;

	}

	@Override
	public String getRequestName() {

		return requestName;
	}

	/**
	 * @param requestName
	 *            the requestName to set
	 */
	public void setRequestName(String requestName) {
		this.requestName = requestName;
	}

	@Override
	public Map<String, String> getRequestParams() {
		return requestParams;
	}

	/**
	 * @param requestParams
	 *            the requestParams to set
	 */
	public void setRequestParams(Map<String, String> requestParams) {
		this.requestParams = requestParams;
	}

	@Override
	public String getClientName() {

		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OpenRuleEngineRequest [clientName=" + clientName
				+ ", requestName=" + requestName + ", requestParams="
				+ requestParams + "]";
	}

}
