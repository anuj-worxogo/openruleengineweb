/*
This file is part of Worxogo Rule Engine Wrapper(wre) which is a java webapp.
Copyright © 2016 Worxogo Solutions Pvt Ltd

wre is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

wre is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; If not, see <http://www.gnu.org/licenses/>. 
 */
package com.worxogo.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Instance of the class represents wrapper over system's java compiler
 * ToolProvider.getSystemJavaCompiler() to compile Open Rule's generated java
 * beans.
 * 
 * @author Anuj Bhatnagar
 *
 */
public class RuntimeWrapper {

	private static final FilenameFilter FILE_NAME_FILTER = new FilenameFilter() {

		@Override
		public boolean accept(File dir, String name) {
			return name.lastIndexOf(".java") > 0 ? true : false;
		}
	};
	private static Logger _logger = LogManager.getLogger(RuntimeWrapper.class);
	private static String classPathURL = RuntimeWrapper.class
			.getProtectionDomain().getCodeSource().getLocation().getPath();
	private static String GENERATED_CLASS_PATH = "openrules.generated_class.path";

	protected JavaCompiler _compiler;
	// protected URLClassLoader _classLoader;
	private static List<URL> urls = new ArrayList<URL>();
	private static String classPath = "";
	/**
	 * _rootPath comes from property.
	 */
	protected static String _classDirectoryPath = classPathURL
			+ ApplicationUtil.getValue(GENERATED_CLASS_PATH);

	/**
	 * This function builds a classpath from the passed Strings
	 * 
	 * @param paths
	 *            classpath elements
	 * @return returns the complete classpath with wildcards expanded
	 */
	private static void buildClassPath(String... paths) {
		StringBuilder sb = new StringBuilder();
		for (String path : paths) {
			if (path.endsWith("*")) {
				path = path.substring(0, path.length() - 1);
				File pathFile = new File(path);
				for (File file : pathFile.listFiles()) {
					if (file.isFile() && file.getName().endsWith(".jar")) {
						sb.append(path);
						sb.append(file.getName());
						sb.append(System.getProperty("path.separator"));
						try {
							urls.add(new URL("file:" + file.getAbsolutePath()));
						} catch (MalformedURLException e) {

						}
					}
				}
			} else {
				sb.append(path);
				sb.append(System.getProperty("path.separator"));
			}
		}

		classPath = sb.toString();
		_logger.debug("OpenRules' jar for runtime complilation:" + classPath);
	}

	static {
		String jarPath = classPathURL.substring(0,classPathURL.indexOf("WEB-INF")) + "WEB-INF"+"/lib/*";
		_logger.debug("jarPath="+jarPath);
		buildClassPath(jarPath);
	}

	/**
	 * @throws MalformedURLException
	 * 
	 */
	public RuntimeWrapper() {
		_compiler = ToolProvider.getSystemJavaCompiler();
		_logger.debug("System Compiler ::" + _compiler);

	}

	public void generateClass(String clientPackageName)
			throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, MalformedURLException {
		_logger.debug("generateClass  for clientPackageName::"
				+ clientPackageName);

		File diectory = new File(_classDirectoryPath + clientPackageName
				+ RuleEngineConstant.FORWARD_SLASH);
		if (diectory.isDirectory())
			for (String fileName : diectory.list(FILE_NAME_FILTER)) {
				String filePath = diectory.getAbsolutePath()
						+ RuleEngineConstant.FORWARD_SLASH + fileName;
				_compiler.run(null, null, null, new File(filePath).getPath(),
						"-classpath", classPath);
				_logger.debug("Compiled class::" + filePath);

			}

	}

	public void deleteGeneratedClasses(String clientPackageName) throws Exception {
		_logger.debug("deleteGeneratedClasses  for clientPackageName::"
				+ clientPackageName);
		File diectory = null;
		if (null == clientPackageName) {
			diectory = new File(_classDirectoryPath
					+ RuleEngineConstant.FORWARD_SLASH);
		} else {
			diectory = new File(_classDirectoryPath + clientPackageName
					+ RuleEngineConstant.FORWARD_SLASH);
		}

		if (diectory.isDirectory()) {
			try {
				_logger.debug("Deleting  diectory::"
						+ diectory.getAbsolutePath());
				FileUtils.deleteDirectory(diectory);
			} catch (IOException e) {
				e.printStackTrace();
				throw new Exception("Failed in deleting classed at "
						+ diectory.getAbsolutePath());
			}
		}
	}
}
